from channels.routing import include
from canvas.routing import routes as canvas_routes


routes = [
    include(canvas_routes, path=r"^/canvas/")
]