from os import path
import json
import colorsys
import random


COLORS_PATH = path.join(
    path.dirname(path.abspath(__file__)),
    'colors.json'
)

with open(COLORS_PATH) as f:
    colors = json.load(f)


def random_hexcolor():
    color = random.choice(colors)
    return color['value']
