from channels.routing import route
from canvas.consumers import ws_add, ws_message, ws_disconnect


routes = [
    route("websocket.connect", ws_add),
    route("websocket.receive", ws_message),
    route("websocket.disconnect", ws_disconnect)
]