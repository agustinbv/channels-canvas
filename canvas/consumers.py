import json
from pprint import pprint

from channels import Group
from channels.sessions import channel_session

from canvas.utils import random_hexcolor


@channel_session
def ws_add(message):
    pprint(message.__dict__)
    # Accept the connection
    color = random_hexcolor()
    message.channel_session['color'] = color
    message.reply_channel.send({
        "accept": True,
        "text": json.dumps({
            "action": "welcome",
            "value": {
                "color": color
            }
        })
    })
    # Add to the chat group
    Group("canvas").add(message.reply_channel)


@channel_session
def ws_message(message):
    Group("canvas").send({
        "text": message.content['text'],
    })


@channel_session
def ws_disconnect(message):
    Group("canvas").discard(message.reply_channel)